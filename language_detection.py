from nltk.corpus import stopwords
from nltk import word_tokenize
import operator

def detect_language(text):
    languages = stopwords.fileids()
    tokens = word_tokenize(text)
    lang_occurence = {}
    for l in languages:
        stopwords_in_text = [word for word in stopwords.words(l) if word in tokens]
        lang_occurence[l] = len(stopwords_in_text)
    lang_most_comm_words = max(lang_occurence.items(), key=operator.itemgetter(1))[0]
    return lang_most_comm_words


if __name__=='__main__':
    text1 = 'A bag-of-words model, or BoW for short, is a way of extracting features from text for use in modeling, such as with machine learning algorithms. The approach is very simple and flexible, and can be used in a myriad of ways for extracting features from documents.'
    text2 = '24 août : le président de la république de Nauru, Baron Waqa (photo), perd son siège de député lors des élections législatives, ' \
            'et ne peut donc briguer de troisième mandat à la tête de État. Lionel Aingimea lui succède. 22 août : à la suite de la démission du Premier ministre Ramush Haradinaj, Assemblée du Kosovo vote sa dissolution et la tenue élections anticipées. 17 août : en Afghanistan, un attentat suicide lors dun mariage à Kaboul fait 80 morts et 180 blessés.'
    print(detect_language(text1))
    print(detect_language(text2))


