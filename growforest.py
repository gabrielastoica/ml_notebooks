'''
Grow a forest by following these steps:
Continuing the previous exercise, generate 1,000 subsets of the training set, each containing 100 instances selected randomly. Hint: you can use Scikit-Learn’s ShuffleSplit class for this.
Train one Decision Tree on each subset, using the best hyperparameter values found in the previous exercise. 
Evaluate these 1,000 Decision Trees on the test set. Since they were trained on smaller sets, these Decision Trees will likely perform worse than the first Decision Tree, achieving only about 80% accuracy.
Now comes the magic. For each test set instance, generate the predictions of the 1,000 Decision Trees, and keep only the most frequent prediction (you can use SciPy’s mode() function for this). This approach gives you majority-vote predictions over the test set.
Evaluate these predictions on the test set: you should obtain a slightly higher accuracy than your first model (about 0.5 to 1.5% higher). Congratulations, you have trained a Random Forest classifier!

{'max_depth': 10, 'max_leaf_nodes': 50, 'random_state': 42}
'''

from sklearn.datasets import make_moons
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
import matplotlib.pyplot as plt
from sklearn.tree import DecisionTreeClassifier
from sklearn.model_selection import ShuffleSplit
from sklearn.base import clone
import numpy as np

n_trees = 1000
n_instances = 100

X_moons, y_moons = make_moons(n_samples=100000, noise=0.4, random_state=42)
X_train, X_test, y_train, y_test = train_test_split(X_moons, y_moons, test_size=0.2, random_state=42)
clf = DecisionTreeClassifier(max_depth=10, max_leaf_nodes=50, random_state=42)
         

'''Generate 1,000 subsets of the training set, each containing 100 instances selected randomly. '''    
rs = ShuffleSplit(n_splits=n_trees, test_size=len(X_train) - n_instances, random_state=42)
mini_sets = []
for train_index, test_index in rs.split(X_train):
    X_s_train = X_train[train_index]
    y_s_train = y_train[train_index]
    mini_sets.append((X_s_train, y_s_train))

'''Clone the decision tree classifier for each mini-set - duplicate the estimator in order to use it on multiple datasets '''
forest = [clone(clf) for _ in range(n_trees)]

accuracy_scores = []

'''Train one Decision Tree on each subset, using the best hyperparameter values found above. Evaluate these 1,000 Decision Trees on the test set. '''
for tree, (X_s_train, y_s_train) in zip(forest, mini_sets):
    tree.fit(X_s_train, y_s_train)
    y_pred = tree.predict(X_test)
    accuracy_scores.append(accuracy_score(y_test, y_pred))

print(np.mean(accuracy_scores))

'''For each test set instance, generate the predictions of the 1,000 Decision Trees, and keep only the most frequent prediction
(you can use SciPy's mode() function for this). This gives you majority-vote predictions over the test set.'''

# 2-d numpy array (each row is the prediction of X_test for a different tree)
Y_pred = np.empty([n_trees, len(X_test)], dtype=np.uint8)

#enumerate - iterable returning (tree_index, tree)
for tree_index, tree in enumerate(forest):
    Y_pred[tree_index] = tree.predict(X_test)

#obtain the most frequent values for each instance in all trees predictions
from scipy import stats
voted_prediction, no_of_votes = stats.mode(Y_pred, axis=0)

print(accuracy_score(y_test, voted_prediction[0]))

